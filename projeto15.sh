#!/bin/bash


function copia_para_servidor() {
    source_file=$(zenity --file-selection --title="Selecione um arquivo")
    destination=$(zenity --entry --title="Destino" --text="Digite um destino ou servidor")
    server=$(zenity --entry --title="Digite um servidor" --text="Digite o ip do servidor")

    if [ -n "$source_file" ] && [ -n "$destination" ] && [ -n "$server" ]; then
        scp "$source_file" "$server:$destination"
        if [ $? -eq 0 ]; then
            zenity --info --title="Sucesso" --text="Arquivo copiado com sucesso!"
        else
            zenity --error --title="Erro" --text="Copia do arquivo falhou."
        fi
    fi
}


function copiar_do_servidor() {
    source_file=$(zenity --entry --title="Procure um arquivo do servidor" --text="Procure um arquivo do servidor")
    destination=$(zenity --file-selection --title="Selecione um destino ou servidor")
    server=$(zenity --entry --title="Digite o servidor" --text="Digite o ip do servidor")

    if [ -n "$source_file" ] && [ -n "$destination" ] && [ -n "$server" ]; then
        scp "$server:$source_file" "$destination"
        if [ $? -eq 0 ]; then
            zenity --info --title="Successo" --text="Arquivo copiado do servidor com Sucesso!"
        else
            zenity --error --title="Erro" --text="Falha ao copiar o arquivo."
        fi
    fi
}


function verificar_copia() {
    source_file=$(zenity --file-selection --title="Selecione um arquivo")
    destination=$(zenity --file-selection --title="Selecione um arquivo de destino")

    if [ -n "$source_file" ] && [ -n "$destination" ]; then
        source_md5=$(md5sum "$source_file" | awk '{ print $1 }')
        destination_md5=$(md5sum "$destination" | awk '{ print $1 }')

        if [ "$source_md5" = "$destination_md5" ]; then
            zenity --info --title="Verificando" --text="MD5 hashes match!"
        else
            zenity --info --title="Verificando" --text="MD5 hashes do not match!"
        fi
    fi
}


function visualizar_arquivos_locais() {
    arquivos=$(ls)
    zenity --text-info --title="lista de arquivos locais" --width=400 --height=300 --text="$arquivos"
}


function visualizar_arquivos_remotos() {
    server=$(zenity --entry --title="Digite o servidor" --text="Entre com o ip do servidor")

    if [ -n "$server" ]; then
        files=$(ssh "$server" ls)
        zenity --text-info --title="Lista de arquivos remotos" --width=400 --height=300 --text="$files"
    fi
}


function salvar_host() {
    ip=$(zenity --entry --title="Digite um ip" --text="Digite um ip")
    host=$(zenity --entry --title="Digite um Host" --text="Digite um ip")

    if [ -n "$ip" ] && [ -n "$host" ]; then
        echo "$ip $host" >> hosts.txt
        zenity --info --title="Salvar Host" --text="Host salvo com sucesso!"
    fi
}


function checar_ssh_server() {
    server=$(zenity --entry --title="Digite um Servidor" --text="Digite um ip do servidor")

    if [ -n "$server" ]; then
        ssh -q "$server" exit
        if [ $? -eq 0 ]; then
            zenity --info --title="Servidor SSH" --text="Servidor SSH instalado."
        else
            zenity --info --title="Servidor SSH" --text="Servidor SSH não instalado."
        fi
    fi
}


choice=$(zenity --list --title="Menu principal" --column="Opções" "Copiar cliente/servidor" "Copiar servidor/cliente" "Verificar Copia" "Visualizar os arquivos locais" "Visualizar arquivos remotos" "Salvar Host" "Checar SSH Server" --height=400 --width=300)

case "$choice" in
    "Copiar cliente/servidor")
        copia_para_servidor
        ;;
    "Copiar servidor/cliente")
        copiar_do_servidor
        ;;
    "Verificar Copia")
        verificar_copia
        ;;
    "Visualizar os arquivos locais")
        visualizar_arquivos_locais
        ;;
    "Visualizar arquivos remotos")
        visualizar_arquivos_remotos
        ;;
    "Salvar Host")
        salvar_host
        ;;
    "Checar SSH Server")
        checar_ssh_server
        ;;
    *)
        zenity --error --title="Erro" --text="Opção Invalida."
        ;;
esac

